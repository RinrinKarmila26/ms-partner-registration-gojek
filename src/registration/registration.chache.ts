
import redis from '../common/redis';
import { IKeyPartner } from "./registration.interface";


const getById = async(customerId: string):Promise<IKeyPartner> =>{
    const id = await redis.get(customerId);

    if (!id) {
      throw new Error()
    }
  
    return JSON.parse(id);
  
} 

const registrtionChache = {
    getById
}

export default registrtionChache