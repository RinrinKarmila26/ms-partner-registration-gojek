import Joi from "joi";
import { Request, Response, NextFunction } from "express";

export const registrationPayload = Joi.object({
    name: Joi.string(),
    email: Joi.string(),
    idNumber: Joi.string().required(),
    partnerId: Joi.string()
});

const validatePayload = async (req: Request, res: Response, next: NextFunction) => {
    const result = registrationPayload.validate(req.body);
    if (result.error) {
        return res.json({ errors: result.error });
    }
    return result;
};

const validateRegistration = {
    validatePayload,
};

export default validateRegistration;