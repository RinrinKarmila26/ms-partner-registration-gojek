import mongoose, { Schema, Document, Model } from 'mongoose';
import { IRegistModel } from './registration.interface';

export type RegistrationDoc = IRegistModel & Document;

export const collectionName = 'cards';

const RegistrationSchema = new Schema(
  {
    name: {
      type: String,
      required: false
    },
    email: {
      type: String,
      required: false
    },
    idNumber: {
      type: String,
      required: true
    },
    partnerId: {
        type: String,
        required: true
      },
    customerId: {
        type: String,
        required: true
    }
  },
  {
    timestamps: true
  }
);

export const RegistrationModel = mongoose.model<RegistrationDoc>(
    collectionName,
    RegistrationSchema
  );
// export const RegistrationModel: Model<RegistrationDoc> = mongoose.model(
//   collectionName,
//   RegistrationSchema
// );
