import express from "express";
import { IRegistModel } from "./registration.interface";
import registrationService from "./registration.service";
import validateRegistration from "./registration.validate";

const registControler = express.Router();

registControler.get('/hi',registrationService.hiGojek);

registControler.get('/get', async (req, res) => {
    const customerId = '0911878917';
    const result = await registrationService.getFindById(customerId);
    if(!result){
        console.log('data not found')
    }
    return res.status(200).send(result);
  })

registControler.get('/all',async (req, res) => {
    const result = await registrationService.getFindAll();
    if(!result){
        console.log('data not found')
    }
    return res.status(200).send(result);
  
});

registControler.post(
    "/create",
    validateRegistration.validatePayload,
     async (req, res) => {
      const payload: IRegistModel = req.body;
      console.log('payload', payload);
      try {
        const result = await registrationService.newData(payload);
          if(!result){
              console.log(result)
          }
          return res.status(201).send(result);
      } catch (error) {
        console.log(error);
      }
    }
  );
  export default registControler;