import { Document, Model } from "mongoose";
import Joi from 'joi';
import schemaValidate from "./registration.validate";
import 'joi-extract-type';

export interface IRegistModel {
    name: string;
    email: string;
    idNumber: string;
    partnerId:string;
    customerId: string;
}
export interface IKeyPartner {
    customerId: string;
  }
  

