import { Request, Response, NextFunction } from "express";
import {  IRegistModel  } from "./registration.interface";
import registrationRepository from "./registration.repository";


const hiGojek = async (req: Request, res: Response) => {
  res.send({
    message: 'ping Welcome to Gojek jago!'
  })
}
//refactor service
const newData = async (dataInput: IRegistModel): Promise<IRegistModel> => {
  const result = await registrationRepository.createData(dataInput);
  if (!result) {
    console.log('cannot create data')
  }
  return result;
};
const getFindById = async (customerId: string): Promise<IRegistModel | null> => {
    const result = await registrationRepository.findById(customerId);
      if (!result) {
        console.log('cannot founnd data')
      }
  return result;
};

const getFindAll = async (): Promise<IRegistModel[]> => {
  const result = await registrationRepository.findAll();
  if (!result) {
    console.log('cannot founnd data')
  }
  return result;
};



const registrationService = {
  newData,
  getFindAll,
  getFindById,
  hiGojek
}


export default registrationService;
