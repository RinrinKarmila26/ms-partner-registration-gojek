import { RegistrationDoc, RegistrationModel } from "./registration.model";
import { IRegistModel } from "./registration.interface";

const convertRegistDocumentToObject = (document: RegistrationDoc) =>
  document.toObject({ getters: true }) as IRegistModel;
const mapConvertRegistToArray = (
  documents: RegistrationDoc[]
) => documents.map(convertRegistDocumentToObject);

const createData = async (registInput: IRegistModel): Promise<IRegistModel> => {
  const document = await RegistrationModel.create(registInput);
  return convertRegistDocumentToObject(document);
};

const findById = async (customerId: string): Promise<IRegistModel | null> => {
  const result = await RegistrationModel.findOne({ _id: customerId });

  return result && convertRegistDocumentToObject(result);
};
const findAll = async (): Promise<IRegistModel[]> => {
  const result = await RegistrationModel.find();

  return result && mapConvertRegistToArray(result);
};

const registrationRepository = {
  findById,
  createData,
  findAll
};
export default registrationRepository;