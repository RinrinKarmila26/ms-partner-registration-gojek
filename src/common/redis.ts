import Redis from 'ioredis';

const redis = Redis();

export default redis;