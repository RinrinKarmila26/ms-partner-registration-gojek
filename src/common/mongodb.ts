import mongoose from 'mongoose';
// import { MongoError } from 'mongodb';

const dbUri = 'mongodb://myUserAdmin:abc123@localhost:27017/admin';
const dbName = 'card_db'

const connectToMongo = () => {
    try {
         mongoose.connect(dbUri, {
            useNewUrlParser: true,   
            useUnifiedTopology: true,
            useCreateIndex: true,
            dbName
        });
        console.log("Successfully connected to the database");
    } catch (err) {
        console.error(err);
        console.log("Could not connect to the database. Error...", err);
        process.exit();
    }
};

//handle for err duplicate

// export const mongoErrorTranslator = (error: MongoError) => {
//   let translatedError: Error;
//   if (
//     error.name === MONGO_ERROR &&
//     error.code === MONGO_ERROR_CODE.DUPLICATED_KEY
//   ) {
//     translatedError = new Error(DUPLICATE_KEY);
//   } else {
//     translatedError = new Error(error.message);
//     translatedError.name = error.name;
//   }

//   console.error('Mongo creation error', error);
//   return translatedError;
// };

export { connectToMongo };
