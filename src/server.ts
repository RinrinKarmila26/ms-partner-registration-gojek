import express from "express";
import { connectToMongo } from "./common/mongodb";
import { routes } from "./routes";
import  bodyParser from 'body-parser';
const port = 3000;
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/registration/register',routes);

 /* conection to mongodb */
 connectToMongo();


//Server
app.listen(port, ()=>{
    console.log('Server is Up, in port', port)
  })

