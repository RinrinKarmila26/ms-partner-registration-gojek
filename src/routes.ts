import registControler from "./registration/registration.controler";
import serverCheckControler from "./servercheck/server.check.controler";


const routes = [
    serverCheckControler,
    registControler
];
export { routes }